-- This file should undo anything in `up.sql`

CREATE SEQUENCE streams_user_id_seq;


UPDATE Streams SET user_id = nextval('streams_user_id_seq')
WHERE user_id IS NULL;

