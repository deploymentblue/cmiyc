-- Your SQL goes here

CREATE TABLE Streams (
  id BIGSERIAL PRIMARY KEY,
  user_id BIGSERIAL NOT NULL,
  image_url TEXT NOT NULL,
  extension varchar(10) NOT NULL,
  processed BOOLEAN NOT NULL,
  fascism_scale double precision NOT NULL
)


