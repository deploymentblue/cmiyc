-- This file should undo anything in `up.sql`

ALTER TABLE Streams DROP CONSTRAINT fk_stream_type;
ALTER TABLE Streams DROP COLUMN stream_type;
ALTER TABLE Streams ADD COLUMN stream_type varchar(56) NOT NULL DEFAULT 'TWITTER';
DROP TABLE StreamType;