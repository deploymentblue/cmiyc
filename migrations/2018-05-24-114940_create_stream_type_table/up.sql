-- Your SQL goes here

ALTER TABLE Streams DROP COLUMN stream_type;

CREATE TABLE StreamType (
  id SERIAL PRIMARY KEY,
  type_name varchar(30) NOT NULL

);

INSERT INTO StreamType (type_name) VALUES('TWITTER');

ALTER TABLE Streams ADD COLUMN stream_type INT NOT NULL DEFAULT 1;

ALTER TABLE Streams
ADD CONSTRAINT fk_stream_type
FOREIGN KEY (stream_type)
REFERENCES StreamType(id)
ON UPDATE CASCADE ON DELETE CASCADE;