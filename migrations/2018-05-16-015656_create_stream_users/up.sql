-- Your SQL goes here

CREATE TABLE StreamUsers (
  id BIGSERIAL PRIMARY KEY,
  stream_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  profile_name TEXT NOT NULL,
  location varchar(10),
  handle TEXT NOT NULL
);


ALTER TABLE StreamUsers
ADD CONSTRAINT fk_userId
FOREIGN KEY (stream_id)
REFERENCES Streams(id)
ON UPDATE CASCADE ON DELETE CASCADE;

