# README #
Catch Me If You Can is an application that gets data from twitter and sends it over to the authenatication service

### What is this repository for? ###

* This repo contains source code for Catch Me If You can

### How do I get set up? ###

* Needs Postgresql 9 and above
* Needs Diesel Framework to work, run setup and migrations
* Set up DATABASE_URL environment variable to contain database url
* Set up TWITTER_CONSUMER_KEY environment variable to contain twitter api consumer key
* Set up TWITTER_CONSUMER_SECRET environment variable to contain twitter api consumer secret
* Set up TWITTER_ACCESS_TOKEN environment variable to contain twitter api access token
* Set up TWITTER_ACCESS_SECRET environment variable to contain twitter api access secret
* Set up IDENTIFICATION_SERVICE environment variable to contain url for the nemesis servivce

