#![recursion_limit = "128"]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_codegen;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

pub mod core;
