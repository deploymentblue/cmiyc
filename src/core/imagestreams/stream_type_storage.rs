use core::common;
use core::imagestreams::stream_type;
use core::schema::streamtype;
use core::schema::streamtype::dsl::*;
use diesel;
use diesel::prelude::*;
use diesel::result::QueryResult;
use diesel::FilterDsl;
use diesel::LoadDsl;
use diesel::PgConnection;
use diesel::SaveChangesDsl;

pub fn get_stream_type_by_id<'a>(
    connection: &'a PgConnection,
    type_id: i32,
) -> Result<stream_type::StreamType, common::CommonError> {
    let results = streamtype::table
        .filter(id.eq(type_id))
        .load::<stream_type::StreamType>(connection);
    match results {
        Ok(diesel_result) => match diesel_result.first() {
            Some(first) => Ok(first.clone()),
            None => Err(common::CommonError {
                error_code: 1234,
                error_message: String::from("EMPTY RESULT FROM DB"),
            }),
        },
        Err(_) => Err(common::CommonError {
            error_code: 1234,
            error_message: String::from("COULD NOT GET FROM DB"),
        }),
    }
}

pub fn get_stream_type_by_name<'a>(
    connection: &'a PgConnection,
    stream_name: String,
) -> Result<stream_type::StreamType, common::CommonError> {
    let results = streamtype::table
        .filter(type_name.eq(stream_name))
        .load::<stream_type::StreamType>(connection);
    match results {
        Ok(diesel_result) => match diesel_result.first() {
            Some(first) => Ok(first.clone()),
            None => Err(common::CommonError {
                error_code: 1234,
                error_message: String::from("EMPTY RESULT FROM DB"),
            }),
        },
        Err(_) => Err(common::CommonError {
            error_code: 1234,
            error_message: String::from("COULD NOT GET FROM DB"),
        }),
    }
}
