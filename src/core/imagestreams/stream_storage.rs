use core::common;
use core::imagestreams::stream;
use core::imagestreams::stream_type;
use core::imagestreams::stream_type_storage;

use core::schema::streams;
use core::schema::streams::dsl::*;
use diesel;
use diesel::prelude::*;
use diesel::result::QueryResult;
use diesel::FilterDsl;
use diesel::LoadDsl;
use diesel::PgConnection;
use diesel::SaveChangesDsl;

#[derive(Debug)]
pub enum StreamStoreStatus {
    Ok,
}

struct StreamStorageMock {
    mock_stream_storage: Vec<stream::Stream>,
}

pub trait StreamStorage {
    fn store_stream(
        &self,
        stream_to_insert: &stream::Stream,
    ) -> Result<StreamStoreStatus, common::CommonError>;
    fn get_streams(&self) -> Result<Vec<stream::Stream>, common::CommonError>;
    fn get_stream_by_id(&self, post_id: i64) -> Result<stream::Stream, common::CommonError>;
    fn get_new_stream_by_id(&self, post_id: i64) -> Result<stream::NewStream, common::CommonError>;
    fn update_stream(
        &self,
        stream_to_update: &stream::NewStream,
    ) -> Result<StreamStoreStatus, common::CommonError>;
}

pub struct StreamStoragePostgresql<'a> {
    pub connection: &'a PgConnection,
}

impl<'a> StreamStorage for StreamStoragePostgresql<'a> {
    fn store_stream(
        &self,
        stream_to_insert: &stream::Stream,
    ) -> Result<StreamStoreStatus, common::CommonError> {
        match stream_type_storage::get_stream_type_by_name(
            self.connection,
            stream_to_insert.stream_type.clone(),
        ) {
            Ok(stream_type_from_db) => {
                let new_stream = stream::NewStream {
                    id: stream_to_insert.id as i64,
                    user_id: match stream_to_insert.user_id {
                        Some(us) => Some(us as i64),
                        None => None,
                    },
                    image_url: stream_to_insert.image_url.clone(),
                    extension: get_extension_from_uri(&stream_to_insert.image_url),
                    processed: false,
                    fascism_scale: 0.0,
                    stream_type: stream_type_from_db.id.clone(),
                };

                let result: QueryResult<stream::NewStream> = diesel::insert(&new_stream)
                    .into(streams::table)
                    .get_result(&*self.connection);
                match result {
                    _ => Ok(StreamStoreStatus::Ok),
                    Err(err) => Err(common::CommonError {
                        error_code: 1234,
                        error_message: String::from("COULD NOT INSERT INTO DB"),
                    }),
                }
            }
            Err(_) => Err(common::CommonError {
                error_code: 1234,
                error_message: String::from("COULD NOT GET STREAM TYPE"),
            }),
        }
    }
    fn get_streams(&self) -> Result<Vec<stream::Stream>, common::CommonError> {
        let results = streams::table.load::<stream::NewStream>(&*self.connection);
        match results {
            Ok(vectors) => Ok(convert_stream_to_new_stream(self.connection, &vectors)),
            Err(err) => Err(common::CommonError {
                error_code: 1234,
                error_message: String::from("COULD NOT GET FROM DB"),
            }),
        }
    }

    fn get_stream_by_id(&self, post_id: i64) -> Result<stream::Stream, common::CommonError> {
        let results = streams::table
            .filter(id.eq(post_id))
            .load::<stream::NewStream>(&*self.connection);
        let twitter_streams = match results {
            Ok(diesel_results) => diesel_results,
            Err(_) => vec![],
        };
        let first_stream = twitter_streams.first();
        match first_stream {
            Some(s) => Ok(stream::Stream {
                id: s.id as u64,
                user_id: match s.user_id {
                    Some(us) => Some(us as u64),
                    None => None,
                },
                image_url: s.image_url.clone(),
                stream_type: String::from("TWITTER"),
            }),
            None => Err(common::CommonError {
                error_code: 1234,
                error_message: String::from("COULD NOT GET FROM DB"),
            }),
        }
    }

    fn get_new_stream_by_id(&self, post_id: i64) -> Result<stream::NewStream, common::CommonError> {
        let results = streams::table
            .filter(id.eq(post_id))
            .load::<stream::NewStream>(&*self.connection);
        let twitter_streams = match results {
            Ok(diesel_results) => diesel_results,
            Err(_) => vec![],
        };
        let first_stream = twitter_streams.first();
        match first_stream {
            Some(s) => Ok(s.clone()),
            None => Err(common::CommonError {
                error_code: 1234,
                error_message: String::from("COULD NOT GET FROM DB"),
            }),
        }
    }

    fn update_stream(
        &self,
        stream_to_update: &stream::NewStream,
    ) -> Result<StreamStoreStatus, common::CommonError> {
        match stream_to_update.save_changes::<stream::NewStream>(&*self.connection) {
            Ok(_) => Ok(StreamStoreStatus::Ok),
            Err(error) => Err(common::CommonError {
                error_code: 1234,
                error_message: String::from("COULD NOT UPDATE INTO DB"),
            }),
        }
    }
}

fn convert_stream_to_new_stream<'a>(
    connection: &'a PgConnection,
    streams_from_db: &Vec<stream::NewStream>,
) -> Vec<stream::Stream> {
    streams_from_db
        .iter()
        .map(|vec| stream::Stream {
            id: vec.id as u64,
            user_id: match vec.user_id {
                Some(us) => Some(us as u64),
                None => None,
            },
            image_url: vec.image_url.clone(),
            stream_type: match stream_type_storage::get_stream_type_by_id(
                connection,
                vec.stream_type.clone(),
            ) {
                Ok(s_type) => s_type.type_name,
                Err(_) => String::from(""),
            },
        })
        .collect()
}

impl StreamStorage for StreamStorageMock {
    fn store_stream(
        &self,
        stream_to_insert: &stream::Stream,
    ) -> Result<StreamStoreStatus, common::CommonError> {
        Ok(StreamStoreStatus::Ok)
    }

    fn get_streams(&self) -> Result<Vec<stream::Stream>, common::CommonError> {
        Ok(common::utility::cloned_stream_vector(
            &self.mock_stream_storage,
        ))
    }

    fn get_stream_by_id(&self, post_id: i64) -> Result<stream::Stream, common::CommonError> {
        Ok(stream::Stream {
            id: 1 as u64,
            user_id: Some(1 as u64),
            image_url: String::from(""),
            stream_type: String::from("TWITTER"),
        })
    }

    fn get_new_stream_by_id(&self, post_id: i64) -> Result<stream::NewStream, common::CommonError> {
        Ok(stream::NewStream {
            id: 1,
            user_id: Some(1),
            image_url: String::from(""),
            extension: String::from(""),
            processed: false,
            fascism_scale: 0.0,
            stream_type: 1,
        })
    }

    fn update_stream(
        &self,
        stream_to_update: &stream::NewStream,
    ) -> Result<StreamStoreStatus, common::CommonError> {
        Ok(StreamStoreStatus::Ok)
    }
}

fn get_extension_from_uri(uri: &str) -> String {
    if uri.contains(".") {
        let mut reverse_split = uri.split(".").map(|s| s).collect::<Vec<_>>();
        reverse_split.reverse();
        match reverse_split.first() {
            Some(image_extension) => String::from(image_extension.clone()),
            None => String::from(""),
        }
    } else {
        String::from("")
    }
}

#[test]
fn test_extension_from_uri() {
    assert_eq!(get_extension_from_uri("https://image.com/stuff.jpg"), "jpg");
}

#[test]
fn test_extension_from_empty_uri() {
    assert_eq!(get_extension_from_uri(""), "");
}

#[test]
fn test_extension_from_single_name_uri() {
    assert_eq!(get_extension_from_uri("image"), "");
}

#[test]
fn test_into_audit_system() {
    let stream1 = stream::Stream {
        id: 1,
        image_url: String::from("urlthis"),
        user_id: Some(2),
        stream_type: String::from("TWITTER"),
    };

    let stream_storage_mock: Box<StreamStorage> = Box::new(StreamStorageMock {
        mock_stream_storage: Vec::new(),
    });
    let expected_streams: Vec<stream::Stream> = Vec::new();

    let stream_store_status = stream_storage_mock.store_stream(&stream1);

    match stream_store_status {
        Ok(StreamStoreStatus::Ok) => assert!(true),
        Err(_) => panic!("TEST WAS EXPECTED STORE STATUS, TEST DID NOT PASS"),
    }
}
#[test]
fn test_get_streams_from_storage() {
    let stream1 = stream::Stream {
        id: 1,
        image_url: String::from("urlthis"),
        user_id: Some(2),
        stream_type: String::from("TWITTER"),
    };
    let stream2 = stream::Stream {
        id: 2,
        image_url: String::from("urlthisAndThat"),
        user_id: Some(3),
        stream_type: String::from("TWITTER"),
    };

    let stream_storage_mock: Box<StreamStorage> = Box::new(StreamStorageMock {
        mock_stream_storage: Vec::new(),
    });
    let mut expected_streams: Vec<stream::Stream> = Vec::new();
    expected_streams.push(stream1.clone());
    expected_streams.push(stream2.clone());

    stream_storage_mock.store_stream(&stream1);
    stream_storage_mock.store_stream(&stream2);

    match stream_storage_mock.get_streams() {
        Ok(streams_from_mock) => {
            common::utility::compare_two_streams(&streams_from_mock, &expected_streams)
        }
        Err(_) => panic!("TEST DID NOT PASS,EXPECTED STREAMS FROM GET STREAMS"),
    }
}
