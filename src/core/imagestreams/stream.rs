use core::schema::streams;

#[derive(Debug, Clone, Queryable, Identifiable, Associations)]
pub struct Stream {
    pub id: u64,
    pub user_id: Option<u64>,
    pub image_url: String,
    pub stream_type: String,
}

#[derive(Clone, Insertable, Queryable, Identifiable, Associations, AsChangeset)]
#[table_name = "streams"]
pub struct NewStream {
    pub id: i64,
    pub user_id: Option<i64>,
    pub image_url: String,
    pub extension: String,
    pub processed: bool,
    pub fascism_scale: f64,
    pub stream_type: i32,
}
