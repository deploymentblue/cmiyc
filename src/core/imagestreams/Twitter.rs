use core::common;
use core::imagestreams::image_search;
use core::imagestreams::stream;
use core::usermetadata::usermetadata;
use core::usermetadata::usermetadatasource;

extern crate egg_mode;
use self::egg_mode::search::{self, ResultType};
use self::egg_mode::tweet;
use self::egg_mode::user;

#[derive(Clone)]
pub struct TwitterStreamSource {
    pub consumer_key: String,
    pub consumer_secret: String,
    pub access_token: String,
    pub access_secret: String,
}

fn createStreamFromTwitter(status: &egg_mode::tweet::Tweet) -> stream::Stream {
    let image_path = match status.clone().entities.media {
        Some(value) => match value.first() {
            Some(first_value) => first_value.media_url_https.clone(),
            None => String::from(""),
        },
        None => String::from(""),
    };
    let user = match status.clone().user {
        Some(id_to_get) => id_to_get.id,
        None => 0,
    };

    stream::Stream {
        id: status.clone().id,
        image_url: image_path.clone(),
        user_id: Some(user.clone()),
        stream_type: String::from("TWITTER"),
    }
}

fn getStatuses<'a>(
    token_with_search: (&egg_mode::Token, egg_mode::search::SearchBuilder<'a>),
) -> Result<Vec<stream::Stream>, egg_mode::error::Error> {
    Ok(token_with_search
        .1
        .call(&token_with_search.0)?
        .statuses
        .iter()
        .map(|status| createStreamFromTwitter(status))
        .collect())
}

fn createToken(source: &TwitterStreamSource) -> egg_mode::Token {
    let consumer_key = egg_mode::KeyPair::new(
        source.consumer_key.to_string(),
        source.consumer_secret.to_string(),
    );
    let access_key = egg_mode::KeyPair::new(
        source.access_token.to_string(),
        source.access_secret.to_string(),
    );
    egg_mode::Token::Access {
        consumer: consumer_key,
        access: access_key,
    }
}

impl usermetadatasource::UserMetaDataSource for TwitterStreamSource {
    fn getUserMetaData(
        &self,
        user_stream: stream::Stream,
    ) -> Result<usermetadata::NewUserMetaData, usermetadata::MetaDataError> {
        let token = createToken(&self);

        match egg_mode::verify_tokens(&token) {
            Ok(_) => match user_stream.user_id {
                Some(user_id) => match egg_mode::user::show(user_id, &token) {
                    Ok(user) => Ok(usermetadata::NewUserMetaData {
                        user_id: user.id as i64,
                        profile_name: user.name.clone(),
                        location: user.location.clone(),
                        handle: user.screen_name.clone(),
                    }),
                    Err(_) => Err(usermetadata::MetaDataError::Error(common::CommonError {
                        error_code: 7000,
                        error_message: String::from("Could not get user information"),
                    })),
                },
                None => Err(usermetadata::MetaDataError::Error(common::CommonError {
                    error_code: 7000,
                    error_message: String::from("Did not provide user id"),
                })),
            },
            Err(_) => Err(usermetadata::MetaDataError::Error(common::CommonError {
                error_code: 5000,
                error_message: String::from("Token is invalid"),
            })),
        }
    }
}

impl image_search::SteamSource for TwitterStreamSource {
    fn search_images(
        &self,
        search_term: &str,
    ) -> Result<Vec<stream::Stream>, image_search::SearchError> {
        let token = createToken(&self);

        match egg_mode::verify_tokens(&token) {
            Ok(_) => {
                let search_query: &str =
                    &*["\"", &(*search_term), "\"", " ", "filter:images"].join("");
                let tweets = search::search(search_query)
                    .result_type(ResultType::Recent)
                    .count(10);
                match getStatuses((&token, tweets)) {
                    Ok(tweets_returned) => Ok(tweets_returned),
                    Err(_) => Err(image_search::SearchError::DatasetError(
                        common::CommonError {
                            error_code: 7010,
                            error_message: String::from("ERROR GRABBING DATA FORM TWITTER SERVICE"),
                        },
                    )),
                }
            }
            Err(_) => Err(image_search::SearchError::DatasetError(
                common::CommonError {
                    error_code: 7000,
                    error_message: String::from("COULD NOT VERIFY TOKEN"),
                },
            )),
        }
    }

    fn get_stream_details(
        &self,
        stream_id: i64,
    ) -> Result<stream::Stream, image_search::SearchError> {
        let token = createToken(&self);
        match egg_mode::verify_tokens(&token) {
            Ok(_) => match egg_mode::tweet::show(stream_id as u64, &token) {
                Ok(tweet) => Ok(createStreamFromTwitter(&tweet)),
                Err(_) => Err(image_search::SearchError::DatasetError(
                    common::CommonError {
                        error_code: 8000,
                        error_message: String::from("COULD NOT GET USER DETAILS"),
                    },
                )),
            },
            Err(_) => Err(image_search::SearchError::DatasetError(
                common::CommonError {
                    error_code: 7000,
                    error_message: String::from("COULD NOT VERIFY TOKEN"),
                },
            )),
        }
    }
}
