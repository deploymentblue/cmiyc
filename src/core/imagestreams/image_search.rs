use core::common;
use core::imagestreams::stream;
extern crate egg_mode;
use self::egg_mode::search::{self, ResultType};
use self::egg_mode::tweet;

#[derive(Debug)]
pub enum SearchError {
    DatasetError(common::CommonError),
}

struct SteamSourceMock {
    streams: Vec<stream::Stream>,
    stream_service_error: bool,
}

pub trait SteamSource {
    fn search_images(&self, search_term: &str) -> Result<Vec<stream::Stream>, SearchError>;
    fn get_stream_details(&self, stream_id: i64) -> Result<stream::Stream, SearchError>;
}

impl SteamSource for SteamSourceMock {
    fn search_images(&self, search_term: &str) -> Result<Vec<stream::Stream>, SearchError> {
        match self.stream_service_error {
            false => Ok(common::utility::cloned_stream_vector(&self.streams)),
            _ => Err(SearchError::DatasetError(common::CommonError {
                error_code: 2000,
                error_message: String::from("Stream SEARCH DID NOT RETURN ANYTHING"),
            })),
        }
    }

    fn get_stream_details(&self, stream_id: i64) -> Result<stream::Stream, SearchError> {
        unimplemented!("did not implement");
    }
}

fn search_for_images(
    stream_service: Box<SteamSource>,
    search_terms: &str,
) -> Result<Vec<stream::Stream>, SearchError> {
    match stream_service.search_images(search_terms) {
        Ok(ref stream_results) if stream_results.len() > 0 => {
            Ok(common::utility::cloned_stream_vector(&stream_results))
        }
        Ok(_) => Err(SearchError::DatasetError(common::CommonError {
            error_code: 2000,
            error_message: String::from("Stream SEARCH DID NOT RETURN ANYTHING"),
        })),
        Err(_) => Err(SearchError::DatasetError(common::CommonError {
            error_code: 5000,
            error_message: String::from("COULD NOT CONNECT TO STREAM SERVICE"),
        })),
    }
}

#[test]
fn test_find_no_tweets() {
    let mock_streams: Vec<stream::Stream> = Vec::new();
    let stream_service: Box<SteamSource> = Box::new(SteamSourceMock {
        streams: mock_streams,
        stream_service_error: false,
    });
    let error_to_test = search_for_images(stream_service, "Stream_data_that_does_not_exist");
    match error_to_test {
        Err(SearchError::DatasetError(error_type)) => {
            assert_eq!(error_type.error_code, 2000);
            assert_eq!(
                error_type.error_message,
                "Stream SEARCH DID NOT RETURN ANYTHING"
            )
        }
        Ok(_) => panic!("TEST DID NOT COMPLETE, EXPECTED SEARCH Error ENUM"),
    }
}

#[test]
fn test_error_when_connecting_to_stream_service() {
    let mock_streams: Vec<stream::Stream> = Vec::new();
    let stream_service: Box<SteamSource> = Box::new(SteamSourceMock {
        streams: mock_streams,
        stream_service_error: true,
    });
    let error_to_test = search_for_images(stream_service, "Stream_data_that_does_not_exist");
    match error_to_test {
        Err(SearchError::DatasetError(error_type)) => {
            assert_eq!(error_type.error_code, 5000);
            assert_eq!(
                error_type.error_message,
                "COULD NOT CONNECT TO STREAM SERVICE"
            )
        }
        Ok(_) => panic!("TEST DID NOT COMPLETE, EXPECTED SEARCH Error ENUM"),
    }
}

#[test]
fn test_find_images_from_search() {
    let mut mock_streams: Vec<stream::Stream> = Vec::new();
    mock_streams.push(stream::Stream {
        id: 1,
        image_url: String::from("findmehere"),
        user_id: Some(1),
        stream_type: String::from("TWITTER"),
    });
    mock_streams.push(stream::Stream {
        id: 2,
        image_url: String::from("findmehere2"),
        user_id: Some(2),
        stream_type: String::from("TWITTER"),
    });

    let stream_service: Box<SteamSource> = Box::new(SteamSourceMock {
        streams: common::utility::cloned_stream_vector(&mock_streams),
        stream_service_error: false,
    });
    let streams = search_for_images(stream_service, "find");
    match streams {
        Ok(stream_from_search) => {
            common::utility::compare_two_streams(&stream_from_search, &mock_streams)
        }
        _ => panic!("DID NOT SATISFY TEST CONDITIONS : TEST FAILED"),
    }
}
