use core::schema::streamtype;

#[derive(Clone, Queryable, Identifiable)]
#[table_name = "streamtype"]
pub struct StreamType {
    pub id: i32,
    pub type_name: String,
}
