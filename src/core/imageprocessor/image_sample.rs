#[derive(Deserialize, Serialize)]
pub struct sample {
    pub id: u64,
    pub byte_array: Vec<u8>,
}
