extern crate dotenv;
extern crate reqwest;

use self::dotenv::dotenv;
use core::common;
use core::imageprocessor::image_sample;
use std::collections::HashMap;
use std::env;
use std::io::{Read, Write};

#[derive(Deserialize, Serialize)]
pub struct identifier_response {
    pub id: u64,
    pub fascism_scale: f64,
}

pub fn sample_image(
    image_to_sample: &image_sample::sample,
) -> Result<identifier_response, common::CommonError> {
    let mut params = HashMap::new();
    params.insert("post_id", image_to_sample);
    params.insert("image_data", image_to_sample);

    let client = reqwest::Client::new();

    let response: Result<reqwest::Response, common::CommonError> =
        match env::var("IDENTIFICATION_SERVICE_LOCATION") {
            Ok(identification_serivce_uri) => match client
                .post(&identification_serivce_uri)
                .json(&params)
                .send()
            {
                Ok(succesful_send) => Ok(succesful_send),
                Err(unsuccesful_send) => Err(common::CommonError {
                    error_code: 5000,
                    error_message: String::from("URI COULD NOT SEND"),
                }),
            },
            Err(_) => Err(common::CommonError {
                error_code: 5000,
                error_message: String::from("URI NOT SET FOR SERVICE"),
            }),
        };

    let response_result: Result<identifier_response, common::CommonError> = match response {
        Ok(mut unwraped_response) => if unwraped_response.status().is_success() {
            match unwraped_response.json::<identifier_response>() {
                Ok(serialized_json) => Ok(serialized_json),
                Err(_) => Err(common::CommonError {
                    error_code: 5000,
                    error_message: String::from("COULD NOT SERIALIZE RESPONSE"),
                }),
            }
        } else {
            Err(common::CommonError {
                error_code: 5000,
                error_message: String::from(
                    [
                        "INVALID RESPONSE FROM SERVICE STATUS",
                        &*unwraped_response.status().to_string(),
                    ].join(""),
                ),
            })
        },
        Err(_) => Err(common::CommonError {
            error_code: 5000,
            error_message: String::from("COULD NOT SERIALIZE JSON"),
        }),
    };

    response_result
}
