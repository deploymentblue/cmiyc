extern crate reqwest;
use core::common;
use std::io::{Read, Write};

pub fn download_from_uri(uri: String) -> Result<Vec<u8>, common::CommonError> {
    match reqwest::get(&uri) {
        Ok(mut resp) => if (resp.status().is_success()) {
            let mut byte_array: Vec<u8> = vec![];
            resp.read_to_end(&mut byte_array);
            Ok(byte_array)
        } else {
            Err(common::CommonError {
                error_code: 900,
                error_message: String::from("COULD NOT DOWNLOAD"),
            })
        },
        Err(_) => Err(common::CommonError {
            error_code: 900,
            error_message: String::from("COULD NOT DOWNLOAD"),
        }),
    }
}
