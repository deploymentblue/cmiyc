use core::imagestreams::stream;

pub fn compare_two_streams(
    streams: &Vec<stream::Stream>,
    streams_to_compare_to: &Vec<stream::Stream>,
) {
    streams
        .iter()
        .zip(streams_to_compare_to.iter())
        .map(|(str1, str2)| {
            assert_eq!(str1.id, str2.id);
            assert_eq!(str1.user_id, str2.user_id);
            assert_eq!(str1.image_url, str2.image_url);
        })
        .collect::<Vec<_>>();
}

pub fn cloned_stream_vector(stream: &Vec<stream::Stream>) -> Vec<stream::Stream> {
    stream.iter().filter(|s| s.id >= 0).cloned().collect()
}
