#[derive(Debug)]
pub struct Error {
    pub error_code: i32,
    pub error_message: String,
}
