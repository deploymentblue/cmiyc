use core::schema::application_options;

#[derive(Clone, Insertable, Queryable, Identifiable, Associations, AsChangeset)]
#[table_name = "application_options"]
pub struct application_option {
    pub id: i64,
    pub key: String,
    pub value: String,
}
