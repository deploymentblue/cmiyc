use core::schema::application_keywords;

#[derive(Clone, Insertable, Queryable, Identifiable, Associations, AsChangeset)]
#[table_name = "application_keywords"]
pub struct application_keyword {
    pub id: i64,
    pub keyword: String,
}
