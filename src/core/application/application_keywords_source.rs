use core::application::application_keyword;
use core::common;
use core::schema::application_keywords;
use core::schema::application_keywords::dsl::*;
use diesel;
use diesel::prelude::*;
use diesel::result::QueryResult;
use diesel::FilterDsl;
use diesel::LoadDsl;
use diesel::PgConnection;
use diesel::SaveChangesDsl;

pub fn get_keyword_by_index(
    connection: &PgConnection,
    keyword_id: i64,
) -> Result<Vec<application_keyword::application_keyword>, common::CommonError> {
    let results = application_keywords::table
        .filter(id.eq(keyword_id))
        .load::<application_keyword::application_keyword>(connection);
    match results {
        Ok(diesel_result) => Ok(diesel_result),
        Err(err) => Err(common::CommonError {
            error_code: 1234,
            error_message: String::from("COULD NOT GET FROM DB"),
        }),
    }
}

pub fn get_all_keywords(
    connection: &PgConnection,
) -> Result<Vec<application_keyword::application_keyword>, common::CommonError> {
    let results =
        application_keywords::table.load::<application_keyword::application_keyword>(connection);
    match results {
        Ok(diesel_result) => Ok(diesel_result),
        Err(err) => Err(common::CommonError {
            error_code: 1234,
            error_message: String::from("COULD NOT GET FROM DB"),
        }),
    }
}
