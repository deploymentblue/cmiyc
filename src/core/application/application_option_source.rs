use core::application::application_option;
use core::common;
use core::schema::application_options;
use core::schema::application_options::dsl::*;
use diesel;
use diesel::prelude::*;
use diesel::result::QueryResult;
use diesel::FilterDsl;
use diesel::LoadDsl;
use diesel::PgConnection;
use diesel::SaveChangesDsl;

#[cfg(test)]
use diesel::debug_query;
#[cfg(test)]
use diesel::pg::Pg;

pub fn get_option_by_key(
    connection: &PgConnection,
    key_to_search: String,
) -> Result<Vec<application_option::application_option>, common::CommonError> {
    let results = application_options::table
        .filter(key.eq(key_to_search))
        .load::<application_option::application_option>(connection);
    match results {
        Ok(diesel_result) => Ok(diesel_result),
        Err(err) => Err(common::CommonError {
            error_code: 1234,
            error_message: String::from("COULD NOT GET FROM DB"),
        }),
    }
}

pub fn update_option_by_key(
    connection: &PgConnection,
    key_to_search: String,
    value_to_set: String,
) -> Result<bool, common::CommonError> {
    let fiter_options = application_options.filter(key.eq(key_to_search));
    let result = diesel::update(fiter_options)
        .set((value.eq(value_to_set)))
        .execute(connection);
    match result {
        Ok(diesel_result) => Ok(true),
        Err(err) => Err(common::CommonError {
            error_code: 1234,
            error_message: String::from("COULD NOT UPDATE ON DB"),
        }),
    }
}
