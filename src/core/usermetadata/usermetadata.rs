use core::common;
use core::imagestreams::stream;
use core::imagestreams::stream::NewStream;
use core::schema::streamusers;

use diesel;
use diesel::prelude::*;
use diesel::result::QueryResult;
use diesel::FilterDsl;
use diesel::LoadDsl;
use diesel::SaveChangesDsl;

pub enum MetaDataError {
    Error(common::CommonError),
}

#[derive(Clone, Queryable, Insertable, Associations)]
#[belongs_to(NewStream)]
#[table_name = "streamusers"]
pub struct NewUserMetaData {
    pub user_id: i64,
    pub profile_name: String,
    pub location: Option<String>,
    pub handle: String,
}
