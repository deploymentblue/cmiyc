use core::schema::streamusers;
use core::schema::streamusers::dsl::*;
use diesel;
use diesel::prelude::*;
use diesel::result::QueryResult;
use diesel::FilterDsl;
use diesel::LoadDsl;
use diesel::SaveChangesDsl;

use core::common;
use core::imagestreams::stream;
use core::usermetadata::usermetadata;

pub trait UserMetaDataSource {
    fn getUserMetaData(
        &self,
        userId: stream::Stream,
    ) -> Result<usermetadata::NewUserMetaData, usermetadata::MetaDataError>;
}

pub fn insertUserMetaData<'a>(
    connection: &'a PgConnection,
    user: usermetadata::NewUserMetaData,
) -> Result<(), usermetadata::MetaDataError> {
    let result = diesel::insert(&user).into(streamusers).execute(connection);
    match result {
        Ok(_) => Ok(()),
        Err(_) => Err(usermetadata::MetaDataError::Error(common::CommonError {
            error_code: 2000,
            error_message: String::from("could not add to database"),
        })),
    }
}
