extern crate env_logger;
extern crate num_cpus;
#[macro_use]
extern crate log;

extern crate CatchMeIfYouCan;
extern crate dotenv;
extern crate uuid;

use self::dotenv::dotenv;
use std::{
    env, fs::File, io::prelude::*, sync::{Arc, Mutex}, thread,
};
use uuid::Uuid;
use CatchMeIfYouCan::core::application::{
    application_keyword, application_keywords_source, application_option, application_option_source,
};
use CatchMeIfYouCan::core::imageprocessor::{identifier, image_sample};
use CatchMeIfYouCan::core::imagestreams::{
    image_search, image_search::SteamSource, pg_driver, stream, stream_storage,
    stream_storage::StreamStorage, Twitter,
};
use CatchMeIfYouCan::core::usermetadata::{
    usermetadata, usermetadatasource, usermetadatasource::UserMetaDataSource,
};
use CatchMeIfYouCan::core::{common, common::downloader};

fn main() {
    env_logger::init().unwrap();
    println!("---------- BEGIN RUN--------------");
    info!("------------ BEGIN RUN--------------");
    let consumer_secret =
        env::var("TWITTER_CONSUMER_SECRET").expect("TWITTER_CONSUMER_SECRET must be set");
    let consumer_key = env::var("TWITTER_CONSUMER_KEY").expect("TWITTER_CONSUMER_KEY must be set");
    let access_token = env::var("TWITTER_ACCESS_TOKEN").expect("TWITTER_ACCESS_TOKEN must be set");
    let access_secret =
        env::var("TWITTER_ACCESS_SECRET").expect("TWITTER_ACCESS_SECRET must be set");
    let twitter_api = Twitter::TwitterStreamSource {
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        access_token: access_token,
        access_secret: access_secret,
    };

    let keyword_index = match application_option_source::get_option_by_key(
        &pg_driver::establish_connection(),
        String::from("keyword_index"),
    ) {
        Ok(options) => match options.first() {
            Some(first_option) => match first_option.value.parse::<i64>() {
                Ok(result) => result,
                Err(_) => 0,
            },
            None => 0,
        },
        Err(_) => 0,
    };

    let search_term = match application_keywords_source::get_keyword_by_index(
        &pg_driver::establish_connection(),
        keyword_index,
    ) {
        Ok(keywords) => match keywords.first() {
            Some(first_keywords) => first_keywords.keyword.clone(),
            None => String::from(""),
        },
        Err(_) => String::from(""),
    };

    println!("Search Term is {:?} for this run", search_term);
    info!("Search Term is {} for this run", search_term);
    let shared_api = Arc::new(twitter_api);
    println!("Num of cpus is {}",num_cpus::get());
    if num_cpus::get()>1
    {
        let get_tweets_arc = shared_api.clone();
        let run_db_arc = shared_api.clone();

        let samples_thread_result = thread::spawn(move || -> () {
            collect_tweets(get_tweets_arc,search_term)
        });
        let db_result = thread::spawn(move || -> () 
        {
            run_db(run_db_arc)
        });
        samples_thread_result.join();
        db_result.join();
    }
    else 
    {
        collect_tweets(shared_api.clone(), search_term);
        run_db(shared_api);
    }
   
    let all_keywords =
        match application_keywords_source::get_all_keywords(&pg_driver::establish_connection()) {
            Ok(keywords) => keywords,
            Err(_) => vec![],
        };

    let new_keyword_index = (keyword_index % all_keywords.len() as i64) + 1;
    let option = application_option_source::update_option_by_key(
        &pg_driver::establish_connection(),
        String::from("keyword_index"),
        new_keyword_index.to_string(),
    );
    match option {
        Ok(_) => {}
        Err(_) => println!("COULD NOT UPDATE OPTION"),
    };
    println!("----------END RUN ------------");
    info!("------------ END RUN--------------");
}

fn sample_all_images(
    tweets_from_db: Vec<stream::Stream>,
) -> Vec<Result<identifier::identifier_response, common::CommonError>> {
    tweets_from_db
        .iter()
        .map(
            |t| match downloader::download_from_uri(t.image_url.clone()) {
                Ok(bytes) => {
                    let sample = image_sample::sample {
                        id: t.id,
                        byte_array: bytes,
                    };
                    identifier::sample_image(&sample)
                }
                Err(download_error) =>{println!("Could not download from stream {}",t.id); Err(download_error)}
            },
        )
        .collect::<Vec<_>>()
}

fn is_tweet_in_database(
    tweet_db: &(
        &stream_storage::StreamStoragePostgresql,
        &mut stream::Stream,
    ),
) -> bool {
    match tweet_db.0.get_stream_by_id(tweet_db.1.id as i64) {
        Ok(in_db) => true,
        Err(_) => false,
    }
}


fn collect_tweets(shared_api:Arc<Twitter::TwitterStreamSource>,search_term:String)
{
    let scoped_twitter_api = shared_api.clone();
    let pg_driver_connection = pg_driver::establish_connection();
        let pg_db = stream_storage::StreamStoragePostgresql {
            connection: &pg_driver_connection,
        };
        let mut tweets = scoped_twitter_api
            .search_images(&*search_term)
            .expect("COULD NOT GET TWEETS AT THIS MOMENT");
        tweets
            .iter_mut()
            .map(|tweet| {
                tweet.user_id = None;
                let mut tweet_store = (&pg_db, &mut tweet.clone());
                if !tweet.image_url.is_empty() && !is_tweet_in_database(&mut tweet_store) {
                    pg_db.store_stream(tweet);
                    println!("Stored Tweet {}",tweet.id);
                } else {
                }
            })
            .collect::<Vec<_>>();
        println!("Collected {} tweets for this run", tweets.clone().len());
        info!("Collected {} tweets for this run", tweets.clone().len());
}

fn run_db(scoped_twitter_api:Arc<Twitter::TwitterStreamSource>)
{

    let pg_db = stream_storage::StreamStoragePostgresql {
            connection: &pg_driver::establish_connection(),
        };
        let sampled_streams: Vec<
            Result<identifier::identifier_response, common::CommonError>,
        > = match pg_db.get_streams() {
            Ok(tweets_from_db) => sample_all_images(tweets_from_db),
            Err(err) => {println!("Could not obtain identification for stream");vec![]}
        };

        sampled_streams
            .into_iter()
            .map(|sample| {
                match sample {
                    Ok(response) => {
                        match pg_db.get_new_stream_by_id(response.id as i64) {
                            Ok(mut stream_from_db) => {
                                stream_from_db.processed = true;
                                stream_from_db.fascism_scale = response.fascism_scale;
                                match pg_db.update_stream(&stream_from_db) {
                                    Ok(_) => {
                                        if (response.fascism_scale >= 0.70) {
                                            match scoped_twitter_api.get_stream_details(stream_from_db.id) {
                                                Ok(s) => match pg_db.store_stream(&s) {
                                                    Ok(_) => match pg_db
                                                        .get_stream_by_id(response.id as i64)
                                                    {
                                                        Ok(stream) => match scoped_twitter_api
                                                            .getUserMetaData(stream)
                                                        {
                                                            Ok(user) => {
                                                                usermetadatasource::insertUserMetaData(&pg_driver::establish_connection(),user);

                                                            }
                                                            Err(_) => {
                                                                println!("COULD NOT INSERT USER METADATA");
                                                            }
                                                        },
                                                        Err(_) => {
                                                            println!(
                                                                "COULD NOT GET STREAM FROMD DB"
                                                            );
                                                        }
                                                    },
                                                    Err(_) => {
                                                        println!("COULD NOT STORE STREAM FROMD DB");
                                                    }
                                                },
                                                Err(_) => println!("COULD NOT GET STREAM"),
                                            }
                                        }
                                    }
                                    Err(_) => println!("UPDATE WITH FASCISM SCALE NOT POSSIBLE"),
                                }
                            }
                            Err(err) => {
                                println!("COULD NOT UPDATE STREAM WITH FASCISM SCALE  : {:?}", err);
                            }
                        };
                    }
                    Err(err) => {
                        println!("COULD NOT UPDATE STREAM WITH FASCISM SCALE  : {:?}", err);
                    }
                };
            })
            .collect::<Vec<_>>();
}